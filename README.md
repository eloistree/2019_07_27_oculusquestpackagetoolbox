# How to use: 2019_07_27_OculusQuestPackageToolbox   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.oculusquestpackagetoolbox":"",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.oculusquestpackagetoolbox",                              
  "displayName": "OculusQuestPackageToolbox",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Tool that allow to detect if a hand is not detected anymore (sun, out of range, battery down)",                         
  "keywords": ["Script","Tool","Hand","Quest","Detector"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    