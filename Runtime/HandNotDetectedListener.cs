﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandNotDetectedListener : MonoBehaviour
{
    public Transform m_handRoot;
    public Transform m_handTargeted;
    public float m_decimalPrecision = 1000; //10 cm 100 mm 1000 (1/10mm)
    public Vector3 m_lastPosition;
    public float m_handNotMovingTimer;
 
    void Update()
    {
        Vector3 localHandPosition = m_handRoot.InverseTransformPoint(m_handTargeted.position);
        localHandPosition = RoundTheValuesToIgnoreFloatNoise(localHandPosition);

        if (!HasMoved(localHandPosition))
        {
            m_handNotMovingTimer += Time.deltaTime;
        }
        else
        {
            m_handNotMovingTimer = 0;
        }

        m_lastPosition = localHandPosition;
        Debug.Log("Hello modification :)");
    }

    private Vector3 RoundTheValuesToIgnoreFloatNoise(Vector3 localHandPosition)
    {
        localHandPosition.x = ((int)(localHandPosition.x * m_decimalPrecision)) / m_decimalPrecision;
        localHandPosition.y = ((int)(localHandPosition.y * m_decimalPrecision)) / m_decimalPrecision;
        localHandPosition.z = ((int)(localHandPosition.z * m_decimalPrecision)) / m_decimalPrecision;
        return localHandPosition;
    }

    private bool HasMoved(Vector3 localHandPosition)
    {
        return m_lastPosition != localHandPosition;
    }
}
